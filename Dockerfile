FROM python:3.13-alpine3.21

ARG openjdk=openjdk8

COPY requirements.txt /requirements.txt

RUN adduser -S -u 1001 -G root -h /home/gradle gradle && \
    chown -R 1001:0 /home/gradle && \
    chmod -R g=u /home/gradle && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd && \
    mkdir -p /data && \
    chown -R 1001:0 /data && \
    chmod -R g=u /data && \
    apk update && apk upgrade && \
    apk add bash tree wget curl zip unzip ${openjdk} postgresql16-client \
            perl-template-toolkit xmlstarlet samba-client jq yq \ 
            gdal-tools gdal-driver-PG gdal-driver-png gdal-driver-jpeg \
            gcc musl-dev libffi-dev libpq-dev python3-dev g++ git mdbtools-odbc && \
    curl -s https://rclone.org/install.sh | bash && \
    rm -rf /tmp/* && \
    pip3 install --upgrade pip wheel setuptools && \
    pip3 install -r requirements.txt && \
    apk del g++ python3-dev musl-dev gcc

WORKDIR /data
COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/
COPY --chown=1001:0 gretl /usr/local/bin/
COPY --chown=1001:0 build.gradle /data/
COPY --chown=1001:0 init.gradle /home/gradle/
COPY --chown=1001:0 odbcinst.ini /etc/

ARG gradle_version=6.9

ENV GRADLE_HOME=/opt/gradle-${gradle_version}
ENV GRADLE_USER_HOME=/home/gradle
ENV ILI_CACHE=/tmp

RUN wget -q https://services.gradle.org/distributions/gradle-${gradle_version}-bin.zip && \
    unzip gradle-${gradle_version}-bin.zip -d /opt/ && \
    rm -f gradle-${gradle_version}-bin.zip && \
    mv /opt/gradle-${gradle_version} /opt/gradle && \
    rm -f /opt/gradle/bin/gradle.bat && \
    ln -s /opt/gradle/bin/gradle /usr/bin/ && \
    chgrp -R 0 /opt/gradle && \
    chmod -R g=u /opt/gradle

USER 1001

RUN gradle --init-script /home/gradle/init.gradle --info --stacktrace resolveAllDependencies && \
    rm -rf /data/* /tmp/* && \
    chown -R 1001:0 /home/gradle && \
    chmod -R g=u /home/gradle && \
    chown -R 1001:0 /data && \
    chmod -R g=u /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/usr/bin/gradle", "--version"]
